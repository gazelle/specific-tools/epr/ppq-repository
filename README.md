
# EPR PPQ Repository MockUp

## Overview

The EPR PPQ Repository MockUp is a SoapUI webservice (mock) that provides Privacy Policy Query Service.

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net:8091/ppq-repository?wsdl
* default_path: /ppq-repository
* default_port: 8092
* default_mock_name: PpqPolicyAdministrationSoapBinding_MockService
* default_mock_path: /opt/simulators/epr-ppq-repository-mockup
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/epr-ppq-repository.log
* default_init.d: /etc/init.d/ppqRepositoryMock
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR Assertion Provider MockUp

### Get the Subversion project

```bash
svn checkout https://scm.gforge.inria.fr/anonscm/svn/gazelle/EPD/trunk/epr-ppq-repository-mockup/ $EPR_PPQ_MOCK_DIR
```

### Install libraries required by SoapUI

Copy the external jars (esapi, velocity and postgresql)

```bash
cp $EPR_PPQ_MOCK_DIR/external_jar/esapi-2.1.0.1.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_PPQ_MOCK_DIR/external_jar/velocity-1.7.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_PPQ_MOCK_DIR/external_jar/postgresql-9.3-1102.jdbc4.jar $SOAPUI_INSTALL_DIR/lib/
```

### Prepare the database

Database is not mandatory, but if not configured, it will raise log errors for each recieving request.
The Database is shared with the ADR Provider Mock project. Schema and data import are then in ADR Provider mock project.

```bash
psql -U gazelle postgres
> CREATE DATABASE "adr" OWNER gazelle ;
> \q
psql -U gazelle adr < $EPR_ADR_MOCK_DIR/sql/adr-schema.sql
psql -U gazelle adr < $EPR_ADR_MOCK_DIR/sql/adr-import.sql
```

## Mock as a service

### Prepare the init.d script

Edit the init.d script `$EPR_PPQ_MOCK_DIR/init.d/ppqRepositoryMock` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_PPQ_MOCK_DIR/init.d/ppqRepositoryMock /etc/init.d/ppqRepositoryMock
sudo chmod u+x /etc/init.d/ppqRepositoryMock
sudo chmod 775 /etc/init.d/ppqRepositoryMock
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d ppqRepositoryMock defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/ppqRepositoryMock start
```

To stop the mock

```bash
sudo /etc/init.d/ppqRepositoryMock stop
```

To get status of the mock

```bash
sudo /etc/init.d/ppqRepositoryMock status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```

You might need to resolve errors when starting the mock

```bash
sudo mv /root/.soapuios/ /root/.soapuios_old
```
