    // create XmlHolder for request content
import org.apache.commons.codec.binary.Base64;
    import groovy.sql.Sql;

    try {

        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/adr", "gazelle", "gazelle", "org.postgresql.Driver")

        def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)
        // Namespace declaration
        holder.namespaces['hl7'] = 'urn:hl7-org:v3'

        def issueInstance = new Date().format("yyyy-MM-dd") + "T" + new Date().format("HH:mm:ss.SS") + "Z"
        def extension = holder["//*:Request/*:Resource/*:Attribute/*:AttributeValue/hl7:InstanceIdentifier/@extension"]
        def root = holder["//*:Request/*:Resource/*:Attribute/*:AttributeValue/hl7:InstanceIdentifier/@root"]
        def policySetIdReference = holder["//*:PolicySetIdReference"]
        def policyIdReference = holder["//*:PolicyIdReference"]

        if (!searchPolicySet(sql, policySetIdReference, policyIdReference, extension, root)) {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('uuid2', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('uuid3', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('issueInstance', issueInstance)
            return "XACMLPolicyQuery_Resp_Failure"
        } else {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('uuid2', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('uuid3', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('issueInstance', issueInstance)

            return "XACMLPolicyQuery_Resp_Success"
        }


    } catch (Exception e) {
        context.mockService.setPropertyValue('soapError', '');
        return "XACMLPolicyQuery_Resp_Failure"
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    // checkWithExtensionRoot
    /////////////////////////////
    def checkWithExtensionRoot(def sql, def root, def extension) {

        def listPolicySet = []
        def message = ""
        def countSelect = "Select count(*) as nbrPolicySet from policy_set_stack where id_extension like ? and id_root like ? and is_active = true;"
        def selectPolicySet = "Select policyset from policy_set_stack where id_extension like ? and id_root like ? and is_active = true;"
        def selectCommunity = "Select community from policy_set_stack where id_extension like ? and id_root like ? and is_active = true;"

        request = sql.eachRow(countSelect, [extension, root])
                { p -> nbrPolicySet = "${p.nbrPolicySet}" }

        if (nbrPolicySet.toInteger() == 0) {
            context.mockService.setPropertyValue('soapError', 'The PolicySet with the given PolicySetIdReference does not exist');
            sql.close()
            return false;
        } else {

            request = sql.eachRow(selectPolicySet, [extension, root])
                    { row -> listPolicySet << row.toString() }
            request = sql.eachRow(selectCommunity, [extension, root])
                    { p -> community = "${p.community}" }


            for (PolicySetMessage in listPolicySet) {
                PolicySetMessage = PolicySetMessage.replace('[policyset:', "")
                PolicySetMessage = PolicySetMessage.replace(']', "")
                byte[] decoded = PolicySetMessage.decodeBase64()
                PolicySetMessage = new String(decoded)
                message = message + PolicySetMessage

            }

            context.mockService.setPropertyValue('message_policy', message)
            context.mockService.setPropertyValue('patient_community', community)
            return true;
        }
    }

    // checkPolicySetIdReference
    /////////////////////////////
    def checkPolicySetIdReference(def sql, def policySetIdReference, def policyIdReference) {

        def message = ""
        def listPolicySet = []
        if (policySetIdReference ==~ "urn:uuid:+([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})+" || policyIdReference ==~ "urn:uuid:+([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})+") {
            selectCommunityWithPolicySetId = "Select community from policy_set_stack where policyset_id like ? and is_active = true;"
            selectPolicySetWithPolicySetId = "Select policyset from policy_set_stack where policyset_id like ? and is_active = true;"
            countSelect = "Select count(*) as nbrPolicySet from policy_set_stack where policyset_id like ? and is_active = true;"
            if (policySetIdReference ==~ "urn:uuid:+([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})+") {
                policy = policySetIdReference
            } else {
                policy = policyIdReference
            }
        } else {
            selectCommunityWithPolicySetId = "Select community from policy_set_stack where policySetIdReference like ? and is_active = true;"
            selectPolicySetWithPolicySetId = "Select policyset from policy_set_stack where policySetIdReference like ? and is_active = true;"
            countSelect = "Select count(*) as nbrPolicySet from policy_set_stack where policySetIdReference like ? and is_active = true;"
            if (policySetIdReference.toString() != "[]") {
                policy = policySetIdReference
            } else {
                policy = policyIdReference
            }
        }

        request = sql.eachRow(countSelect, [policy]) { p -> nbrPolicySet = "${p.nbrPolicySet}" }

        if (nbrPolicySet.toInteger() == 0) {
            context.mockService.setPropertyValue('soapError', 'The PolicySet with the given PolicySetIdReference does not exist');
            return false

        } else {

            request = sql.eachRow(selectCommunityWithPolicySetId, [policy]) { p -> community = "${p.community}" }
            request = sql.eachRow(selectPolicySetWithPolicySetId, [policy]) { row -> listPolicySet << row.toString() }

            for (PolicySetMessage in listPolicySet) {
                PolicySetMessage = PolicySetMessage.replace('[policyset:', "")
                PolicySetMessage = PolicySetMessage.replace(']', "")
                byte[] decoded = PolicySetMessage.decodeBase64()
                PolicySetMessage = new String(decoded)
                message = message + PolicySetMessage
            }

            context.mockService.setPropertyValue('message_policy', message)
            context.mockService.setPropertyValue('patient_community', community)
            return true;
        }

    }

    // searchPolicySet
    /////////////////////////////
    def searchPolicySet(def sql, def policySetIdReference, def policyIdReference, def extension, def root) {
        def SQL = "Select count(*) as nbrPolicySet from policy_set_stack where id_extension like ? and id_root like ? and is_active = true;"

        if ((policySetIdReference == "" || policySetIdReference == null || policySetIdReference.toString() == '[]') && (policyIdReference == "" || policyIdReference == null || policyIdReference.toString() == '[]')) {
            if ((extension == "" || extension == null || extension.toString() == '[]') && (root == "" || root == null || root.toString() == '[]')) {
                context.mockService.setPropertyValue('soapError', "An extension id, root id or a policySetIdReference must be provided");
                return false;

            } else {

                request = sql.eachRow(SQL, [extension, root])
                        { p -> nbrPolicySet = "${p.nbrPolicySet}" }

                log.info nbrPolicySet

                if (nbrPolicySet.toInteger() <= 0) {
                    context.mockService.setPropertyValue('soapError', "The PolicySet with the given Patient ID does not exist");
                    return false
                } else {
                    if (checkWithExtensionRoot(sql, root, extension)) {
                        return true
                    } else {
                        context.mockService.setPropertyValue('soapError', "An error occurred while searching PolicySet");
                        return false
                    }
                }
            }
        } else {

            if (checkPolicySetIdReference(sql, policySetIdReference, policyIdReference)) {
                return true
            } else {
                return false
            }
        }
    }






