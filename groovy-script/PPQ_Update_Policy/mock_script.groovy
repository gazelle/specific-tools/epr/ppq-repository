    // create XmlHolder for request content
    import groovy.sql.Sql;
    import org.apache.commons.codec.binary.Base64;

    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)

    try {

        // Namespace declaration
        holder.namespaces['hl7'] = 'urn:hl7-org:v3'


        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/adr", "gazelle", "gazelle", "org.postgresql.Driver")

        def policySetIdReferenceList = []
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:access-level:normal")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:access-level:restricted")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:access-level:delegation-and-restricted")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:access-level:delegation-and-normal")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:access-level:full")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:provide-level:normal")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:provide-level:restricted")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:provide-level:secret")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:exclusion-list")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:policy-bootstrap")
        policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:doc-admin")

        ///// Security XUA SAML Assertion Properties
        ///////////////////
        def assertionSubjectRoleCode = holder["//*:Security/*:Assertion/*:AttributeStatement/*:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:subject:role']/*:AttributeValue/*:Role/@code"]
        def assertionSubjectNameId = holder["//*:Security/*:Assertion/*:Subject/*:NameID"]
        def assertionSubjectResourceId = holder["//*:Security/*:Assertion/*:AttributeStatement/*:Attribute[@Name='urn:oasis:names:tc:xacml:2.0:resource:resource-id']/*:AttributeValue"]

        def homeCommunity = holder["//*:UpdatePolicyRequest/*:Assertion/*:Issuer"]
        def subjectQualifier = holder["//*:Subject/*:SubjectMatch[*:SubjectAttributeDesignator[@AttributeId=\'urn:oasis:names:tc:xacml:1.0:subject:subject-id-qualifier\']]/*:AttributeValue"]
        def idExtension = holder["//hl7:InstanceIdentifier/@extension"]
        def idRoot = holder["//hl7:InstanceIdentifier/@root"]
        def policySetIdReference = holder["//*:PolicySet/*:PolicySetIdReference"]
        def policySetId = holder["//*:PolicySet/@PolicySetId"]
        def document = holder.getDomNode("//*:PolicySet").toString()

        def subjectId = holder["//*:Subjects/*:Subject/*:SubjectMatch[*:SubjectAttributeDesignator[@AttributeId=\'urn:oasis:names:tc:xacml:1.0:subject:subject-id\']]/*:AttributeValue"]
        def subjectRole = holder["//*:Subjects/*:Subject/*:SubjectMatch[*:SubjectAttributeDesignator[@AttributeId=\'urn:oasis:names:tc:xacml:2.0:subject:role\']]/*:AttributeValue/*:CodedValue/@code"]
        def subjectPurposeOfUse = holder["//*:Subjects/*:Subject/*:SubjectMatch[*:SubjectAttributeDesignator[@AttributeId=\'urn:oasis:names:tc:xspa:1.0:subject:purposeofuse\']]/*:AttributeValue/*:CodedValue/@code"]
        if (subjectId.toString().contains('[]')) {subjectId = ""}

        //Check DATA IF NO EMPTY OR NULL
        /////////////////////////////////
        if (!isAllowedToPerformRequest(sql, assertionSubjectRoleCode, idExtension, idRoot, assertionSubjectResourceId, homeCommunity)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }


        if (!checkSubjectIdentity(sql, subjectId, subjectRole, policySetId)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (!checkSubjectRole(assertionSubjectRoleCode)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (!checkSubjectQualifier(subjectQualifier)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (!checkIdRoot(idRoot)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (!checkIdExtension(idExtension)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (!checkHomeCommunity(homeCommunity)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (!checkPolicySetId(policySetId)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (!isValidPolicySetIdReference(policySetIdReferenceList, policySetIdReference)) {
            return "PPQ_Add_Policy_Resp_Failure"
        }

        if (!checkPolicySetIdReference(policySetIdReference)) {
            return "PPQ_Update_Policy_Resp_Failure"
        }

        if (matchPolicyLevel(policySetIdReference, policySetId, assertionSubjectRoleCode)) {


            if (!countDBPolicySetId(sql, policySetId, idRoot, idExtension, homeCommunity)) {
                return "PPQ_Update_Policy_Resp_Failure"
            } else {


                if (updatePolicySet(sql, idRoot, idExtension, policySetId, homeCommunity, policySetIdReference, document)) {
                    return "PPQ_Update_Policy_Resp_Success"
                } else {
                    requestContext.soapError = "<soap:Reason><soap:Text xml:lang='en'>An error occurred while saving the policySet</soap:Text></soap:Reason>"
                    return "PPQ_Update_Policy_Resp_Failure"
                }
            }

        } else {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', "<soap:Reason><soap:Text xml:lang='en'>This user '" + assertionSubjectRoleCode + "' doesn't have rights for this policySet</soap:Text></soap:Reason>");
            return "PPQ_Update_Policy_Resp_Failure"
        }

    } catch (Exception e) {
        context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
        context.mockService.setPropertyValue('soapError', "<soap:Reason><soap:Text xml:lang='en'>An error occurred while saving the policySet</soap:Text></soap:Reason>");
        return "PPQ_Update_Policy_Resp_Failure"
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* ====================== Method List ======================
     *
     * - matchPolicyLevel(PolicySetRef, PolicyId) Check if Policy level and access right are matching
     * - checkConfidentialityCode(confidentialityCode, confidentialityCodeName) Check if confidentialityCode and status are matching
     * - checkValueAction(action, PolicySetRef) Check if Action are matching with his access level
     * - checkSubject(role, qualifier, id) Check subject information
    */

    /////// Method handling Add PPQ
    ///////////////////////////////////////
    def matchPolicyLevel(def PolicySetRef, def PolicySetId, def role) {

        //// For access-level
        /////////////////////
        if (PolicySetRef.contains('access-level') || PolicySetId.contains('access-level')) {
            if (PolicySetRef.contains('delegation') || PolicySetId.contains('delegation')) {
                if (role.contains("ASS") || role.contains("HCP") || role.contains("REP") || role.contains("PAT") || role.contains("DADM")) {
                    return true;
                } else {
                    return false;
                }
            } else if (PolicySetRef.contains('normal') || PolicySetId.contains('normal')) {
                if (role.contains("HCP") || role.contains("ASS") || role.contains("REP") || role.contains("PAT") || role.contains("DADM")) {
                    return true;
                } else {
                    return false;
                }
            } else if (PolicySetRef.contains('restricted') || PolicySetId.contains('restricted')) {
                if (role.contains("HCP") || role.contains("ASS") || role.contains("REP") || role.contains("PAT") || role.contains("DADM")) {
                    return true;
                } else {
                    return false;
                }
            } else if (PolicySetRef.contains('full') || PolicySetId.contains('full')) {
                if (role.contains("REP") || role.contains("PAT")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

            ///// For provide-level
            ///////////////////////
        } else if (PolicySetRef.contains('provide-level') || PolicySetId.contains('provide-level')) {
            if (PolicySetRef.contains('normal') || PolicySetId.contains('normal')) {
                if (role.contains("HCP") || role.contains("ASS") || role.contains("REP") || role.contains("PAT")) {
                    return true;
                } else {
                    return false;
                }
            } else if (PolicySetRef.contains('restricted') || PolicySetId.contains('restricted')) {
                if (role.contains("HCP") || role.contains("ASS") || role.contains("REP") || role.contains("PAT")) {
                    return true;
                } else {
                    return false;
                }
            } else if (PolicySetRef.contains('full') || PolicySetId.contains('full')) {
                if (role.contains("REP") || role.contains("PAT")) {
                    return true;
                } else {
                    return false;
                }
            } else if (PolicySetRef.contains('secret') || PolicySetId.contains('secret')) {
                if (role.contains("PAT")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

            ///// For exclusion-list
            /////////////////////////
        } else if (PolicySetRef.contains('exclusion-list') || PolicySetId.contains('exclusion-list')) {
            if (role.contains("REP") || role.contains("PAT")) {
                return true;
            } else {
                return false;
            }

            ///// For policy-bootstrap
            //////////////////////////
        } else if (PolicySetRef.contains('policy-bootstrap') || PolicySetId.contains('policy-bootstrap')) {
            if (role.contains("PADM")) {
                return true;
            } else {
                return false;
            }

            ///// For doc-admin
            ///////////////////
        } else if (PolicySetRef.contains('doc-admin') || PolicySetId.contains('doc-admin')) {
            if (role.contains("DADM")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // Check checkSubjectRole
    /////////////////////////////////
    boolean checkSubjectRole(def subjectRole) {
        if (subjectRole == "" || subjectRole == null || subjectRole.toString() == '[]') {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The subject:role must be provide</soap:Text></soap:Reason>')
            return false;
        }
        return true;
    }

    // Check checkSubjectQualifier
    /////////////////////////////////
    boolean checkSubjectQualifier(def subjectQualifier) {
        if (subjectQualifier == "" || subjectQualifier == null || subjectQualifier.toString() == '[]') {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The subject-id-qualifier must be provide</soap:Text></soap:Reason>')
            return false;
        }
        return true;
    }

    // Check checkIdRoot
    /////////////////////////////////
    boolean checkIdRoot(def idRoot) {
        if (idRoot == "" || idRoot == null || idRoot.toString() == '[]') {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The root-id must be provide</soap:Text></soap:Reason>')
            return false;
        }
        return true;
    }

    // Check checkIdExtension
    /////////////////////////////////
    boolean checkIdExtension(def idExtension) {
        if (idExtension == "" || idExtension == null || idExtension.toString() == '[]') {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The extension-id must be provide</soap:Text></soap:Reason>')
            return false;
        }
        return true;
    }

    // Check checkHomeCommunity
    /////////////////////////////////
    boolean checkHomeCommunity(def homeCommunity) {
        if (homeCommunity == "" || homeCommunity == null || homeCommunity.toString() == '[]') {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The homeCommunity must be provide</soap:Text></soap:Reason>')
            return false;
        }
        return true;
    }

    // Check checkPolicySetId
    /////////////////////////////////
    boolean checkPolicySetId(def policySetId) {
        if (policySetId == "" || policySetId == null || policySetId.toString() == '[]') {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The policySetId must be provide</soap:Text></soap:Reason>')
            return false;
        }
        return true;
    }

    // Check checkPolicySetIdReference
    /////////////////////////////////
    boolean checkPolicySetIdReference(def policySetIdReference) {
        if (policySetIdReference == "" || policySetIdReference == null || policySetIdReference.toString() == '[]') {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The policySetIdReference must be provide</soap:Text></soap:Reason>')
            return false;
        }
        return true;
    }

    // Check countDBPolicySetId
    /////////////////////////////////
    boolean countDBPolicySetId(def sql, def policySetId, def idRoot, def idExtension, def homeCommunity) {
        def SQL = "Select count(*) as nbrIdPolicySet from policy_set_stack where policySet_id like ? and id_root like ? and id_extension like ? and community like ? and is_active = true;"
        request = sql.eachRow(SQL, [policySetId, idRoot, idExtension, homeCommunity])
                { p -> nbrIdPolicySet = "${p.nbrIdPolicySet}" }

        if (nbrIdPolicySet.toInteger() != 1) {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">This policySetId"' + policySetId + '" does not exist</soap:Text></soap:Reason>')
            return false;
        } else {
            return true;
        }
    }

    // Check updatePolicySet
    ///////////////////////////////
    boolean updatePolicySet(
            def sql, def idRoot, def idExtension, def policySetId, def homeCommunity, def policySetIdReference, def document) {
        try {

            def SQL = "UPDATE policy_set_stack SET policyset = ? , policysetidreference = ? where policySet_id like ? and id_root like ? and id_extension like ? and community like ? and is_active = true;"
            document = document.replace('<?xml version="1.0" encoding="UTF-8"?>', '')

            byte[] messageBase64 = Base64.encodeBase64(document.getBytes());
            messageToVal = new String(messageBase64)

            sql.execute(SQL, [messageToVal, policySetIdReference, policySetId, idRoot, idExtension, homeCommunity]);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    // Check SubjectIdentity
    ///////////////////////////////
    boolean checkSubjectIdentity(def sql, def nameId, def getRole, def policySetId) {
        def SQL = "Select count(*) as nbrIdPolicySet from policy_set_stack where subject like ? and subject_role like ? and policyset_id like ? and is_active = true;"

        request = sql.eachRow(SQL, [nameId, getRole, policySetId])
                { p -> nbrIdPolicySet = "${p.nbrIdPolicySet}" }

        if (nbrIdPolicySet.toInteger() != 1) {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">This policySetId "' + policySetId + '" does not exist</soap:Text></soap:Reason>')
            return false;
        } else {
            return true;
        }
    }

    // is Allowed To Perform Request
    ///////////////////////////////
    boolean isAllowedToPerformRequest(def sql, def role, def extension, def root, def resourceId, def homeCommunity) {
        if (role == "HCP" || role == "ASS") {
            if (!policyWithDelegationExist(sql, role, extension, root, homeCommunity)) {
                context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
                context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">This user ' + role + ' is not allowed</soap:Text></soap:Reason>')
                return false
            } else {
                return true
            }
        } else if (role == "PAT" || role == "REP") {
            if (resourceId.contains(extension) && resourceId.contains(root)) {
                return true
            } else {
                context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
                context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">This user ' + role + ' is not allowed</soap:Text></soap:Reason>')
                return false
            }
        } else if (role == "PADM") {
            return true
        } else {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">This user ' + role + ' is not allowed</soap:Text></soap:Reason>')
            return false
        }
    }


    // Check checkSubjectId
    /////////////////////////////////
    boolean checkSubjectId(def subjectId) {

        if (subjectId.toString() == '[]' || null == subjectId) {
                context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
                context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">The subject-id cannot be empty.</soap:Text></soap:Reason>');
                return false;
            }
            return true;
    }

    // Exist policy with delegation for thi patient
    ///////////////////////////////
    def policyWithDelegationExist(def sql, def role, def extension, def root, def homeCommunity) {
        def SQL = "SELECT count(*) as nbrPolicy from policy_set_stack where id_extension = ? and id_root = ? and policysetidreference like '%delegation%' and community = ? and is_active = true;"
        request = sql.eachRow(SQL, [extension, root, homeCommunity])
                { p -> nbrPolicy = "${p.nbrPolicy}" }
        if (nbrPolicy.toInteger() >= 1) {
            return true
        } else {
            return false
        }
    }

    // isValid PolicySetIdReference
    ///////////////////////////////
    boolean isValidPolicySetIdReference(def policySetIdReferenceList, def policySetReferenceProvide) {
        boolean exist = false;
        for (policy in policySetIdReferenceList) {
            if (policy.contains(policySetReferenceProvide)) {
                exist = true;
            }
        }
        if (!exist) {
            context.mockService.setPropertyValue('uuid1', UUID.randomUUID().toString())
            context.mockService.setPropertyValue('soapError', '<soap:Reason><soap:Text xml:lang="en">This policySetIdReference "' + policySetReferenceProvide + '" is unknown</soap:Text></soap:Reason>')
        }
        return exist
    }