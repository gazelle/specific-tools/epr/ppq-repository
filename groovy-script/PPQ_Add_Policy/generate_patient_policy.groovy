///// SUBJECT ID FOR EACH ROLE

def dadmList = []
dadmList.add("7601002467458")

def padmList = []
padmList.add("7601002468963")

def glnList = []
glnList.add('7601000050717')
glnList.add('7601002033572')
glnList.add('7601002469191')
glnList.add('7601002467373')
glnList.add('7601002468282')
glnList.add('7601002466565')

def assList = []
assList.add('7601002467158')
assList.add('7601002466812')
assList.add('7601002462586')

def repList = []
repList.add("761337610455909127")

def tcuList = []
tcuList.add("7601002461111")




///// POLICY SET REFERENCE ID

def policySetListForHCPorASS = []
policySetListForHCPorASS.add("urn:e-health-suisse:2015:policies:access-level:normal")
policySetListForHCPorASS.add("urn:e-health-suisse:2015:policies:access-level:restricted")
policySetListForHCPorASS.add("urn:e-health-suisse:2015:policies:access-level:delegation-and-restricted")
policySetListForHCPorASS.add("urn:e-health-suisse:2015:policies:access-level:delegation-and-normal")
policySetListForHCPorASS.add("urn:e-health-suisse:2015:policies:provide-level:normal")
policySetListForHCPorASS.add("urn:e-health-suisse:2015:policies:provide-level:restricted")
policySetListForHCPorASS.add("urn:e-health-suisse:2015:policies:provide-level:secret")
//policySetIdReferenceList.add("urn:e-health-suisse:2015:policies:exclusion-list")

def policySetListForPATorREP= []
policySetListForPATorREP.add("urn:e-health-suisse:2015:policies:access-level:full")

def policySetListForPADM= []
policySetListForPADM.add("urn:e-health-suisse:2015:policies:policy-bootstrap")

def policySetListForDADM= []
policySetListForDADM.add("urn:e-health-suisse:2015:policies:doc-admin")


//// ROLE CODE

def listRole = []
listRole.add("HCP")
listRole.add("PAT")
listRole.add("ASS")
listRole.add("TCU")
listRole.add("PADM")
listRole.add("DADM")
listRole.add("REP")

def extension = testRunner.testCase.getPropertyValue('extension')
def root = testRunner.testCase.getPropertyValue('root')
def community = testRunner.testCase.getPropertyValue('community')

testRunner.testCase.testSuite.setPropertyValue("id_extension",extension)
testRunner.testCase.testSuite.setPropertyValue("id_root",root)
testRunner.testCase.testSuite.setPropertyValue("community", community)

for(subjectRole in listRole){
    if (subjectRole.contains("HCP")) {
        for (gln in glnList) {
            for(policySet in policySetListForHCPorASS){
                testRunner.testCase.testSuite.setPropertyValue("PolicySetId", "urn:uuid:" + UUID.randomUUID().toString())
                testRunner.testCase.testSuite.setPropertyValue("subject-id", gln)
                testRunner.testCase.testSuite.setPropertyValue("subject-role", "HCP")
                testRunner.testCase.testSuite.setPropertyValue("subject-qualifier", "urn:gs1:gln")
                testRunner.testCase.testSuite.setPropertyValue("PolicySetIdReference", policySet)
                testRunner.runTestStepByName("PPQ_ADD_POLICY")
            }
        }
    }
    if (subjectRole.contains("ASS")) {
        for (ass in assList) {
            for(policySet in policySetListForHCPorASS){
                testRunner.testCase.testSuite.setPropertyValue("PolicySetId", "urn:uuid:" + UUID.randomUUID().toString())
                testRunner.testCase.testSuite.setPropertyValue("subject-id", ass)
                testRunner.testCase.testSuite.setPropertyValue("subject-role", "ASS")
                testRunner.testCase.testSuite.setPropertyValue("subject-qualifier", "urn:gs1:gln")
                testRunner.testCase.testSuite.setPropertyValue("PolicySetIdReference", policySet)
                testRunner.runTestStepByName("PPQ_ADD_POLICY")
            }
        }
    }
    if (subjectRole.contains("PAT")) {
            for(policySet in policySetListForPATorREP){
                testRunner.testCase.testSuite.setPropertyValue("PolicySetId", "urn:uuid:" + UUID.randomUUID().toString())
                testRunner.testCase.testSuite.setPropertyValue("subject-id", extension)
                testRunner.testCase.testSuite.setPropertyValue("subject-role", "PAT")
                testRunner.testCase.testSuite.setPropertyValue("subject-qualifier", "urn:e-health-suisse:2015:epr-spid")
                testRunner.testCase.testSuite.setPropertyValue("PolicySetIdReference", policySet)
                testRunner.runTestStepByName("PPQ_ADD_POLICY")
            }
    }
    if (subjectRole.contains("REP")) {
        for (rep in repList) {
            for(policySet in policySetListForPATorREP){
                testRunner.testCase.testSuite.setPropertyValue("PolicySetId", "urn:uuid:" + UUID.randomUUID().toString())
                testRunner.testCase.testSuite.setPropertyValue("subject-id", rep)
                testRunner.testCase.testSuite.setPropertyValue("subject-role", "REP")
                testRunner.testCase.testSuite.setPropertyValue("subject-qualifier", "urn:e-health-suisse:representative-id")
                testRunner.testCase.testSuite.setPropertyValue("PolicySetIdReference", policySet)
                testRunner.runTestStepByName("PPQ_ADD_POLICY")
            }
        }
    }
    if (subjectRole.contains("PADM")) {
        for (padm in padmList) {
            for(policySet in policySetListForPADM){
                testRunner.testCase.testSuite.setPropertyValue("PolicySetId", "urn:uuid:" + UUID.randomUUID().toString())
                testRunner.testCase.testSuite.setPropertyValue("subject-id", padm)
                testRunner.testCase.testSuite.setPropertyValue("subject-role", "PADM")
                testRunner.testCase.testSuite.setPropertyValue("subject-qualifier", "urn:e-health-suisse:policy-administrator-id")
                testRunner.testCase.testSuite.setPropertyValue("PolicySetIdReference", policySet)
                testRunner.runTestStepByName("PPQ_ADD_POLICY")
            }
        }
    }
    if (subjectRole.contains("DADM")) {
        for (dadm in dadmList) {
            for(policySet in policySetListForDADM){
                testRunner.testCase.testSuite.setPropertyValue("PolicySetId", "urn:uuid:" + UUID.randomUUID().toString())
                testRunner.testCase.testSuite.setPropertyValue("subject-id", dadm)
                testRunner.testCase.testSuite.setPropertyValue("subject-role", "DADM")
                testRunner.testCase.testSuite.setPropertyValue("subject-qualifier", "urn:e-health-suisse:document-administrator-id")
                testRunner.testCase.testSuite.setPropertyValue("PolicySetIdReference", policySet)
                testRunner.runTestStepByName("PPQ_ADD_POLICY")
            }
        }
    }
}
