import groovy.sql.Sql;

try {

    // create XmlHolder for request content
    def holder = new com.eviware.soapui.support.XmlHolder(mockRequest.requestContent)

    // Namespace declaration
    holder.namespaces['hl7'] = 'urn:hl7-org:v3'

    def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/adr", "gazelle", "gazelle", "org.postgresql.Driver")

    def homeCommunity = holder["//*:DeletePolicyRequest/*:Assertion/*:Issuer"]
    def NameID = holder["//*:Assertion/*:Subject/*:NameID"]
    def policySetIdReference = holder["//*:Statement/*:PolicySetIdReference"]

    //Check DATA IF NO EMPTY OR NULL
    if (!checkHomeCommunity(homeCommunity)) {
        return "mock_serverFault_generalResponse"
    }
    if (!checkPolicySetIdReference(policySetIdReference)) {
        return "mock_serverFault_generalResponse"
    }
    if (!countDBPolicySetId(sql, policySetIdReference, homeCommunity)) {
        return "mock_serverFault_generalResponse"
    } else {
        if (getIdPolicySet(sql, policySetIdReference, homeCommunity)) {
            return "PPQ_Delete_Policy_Resp_Success"
        } else {
            return "PPQ_Delete_Policy_Resp_Failure"
        }
    }

} catch (Exception e) {
    return "PPQ_Delete_Policy_Resp_Failure"

}

// getIdPolicySet
/////////////////////////////////
boolean getIdPolicySet(def sql, def policySetIdReference, def homeCommunity) {
    def update = "UPDATE policy_set_stack SET is_active = false , policyset = '' where policySet_id = ? and community = ?;"
    sql.execute(update, [policySetIdReference, homeCommunity])
    return true;
}

// Check checkHomeCommunity
/////////////////////////////////
boolean checkHomeCommunity(def homeCommunity) {
    if (homeCommunity == "" || homeCommunity == null || homeCommunity.toString() == '[]') {
        context.mockService.setPropertyValue('soapError', 'The homeCommunity must be provide')
        return false;
    }
    return true;
}

// Check checkPolicySetIdReference
/////////////////////////////////
boolean checkPolicySetIdReference(def policySetIdReference) {
    if (policySetIdReference == "" || policySetIdReference == null || policySetIdReference.toString() == '[]') {
        context.mockService.setPropertyValue('soapError', 'The policySetIdReference must be provide')
        return false;
    }
    return true;
}

// Check countDBPolicySetId
/////////////////////////////////
boolean countDBPolicySetId(def sql, def policySetIdReference, def homeCommunity) {

    def SQL = "Select count(*) as nbrIdPolicySet from policy_set_stack where policySet_id like ? and community like ? and is_active = true;"
    request = sql.eachRow(SQL, [policySetIdReference, homeCommunity])
            { p -> nbrIdPolicySet = "${p.nbrIdPolicySet}" }

    if (nbrIdPolicySet.toInteger() <= 0) {
        context.mockService.setPropertyValue('soapError', 'The PolicySet with the given PolicySet ID does not exist')
        context.mockService.setPropertyValue('detail', '<ppq-policy-administration:UnknownPolicySetId xmlns:ppq-policy-administration="urn:e-health-suisse:2015:policy-administration"/>')
        return false;
    } else {
        return true;
    }
}